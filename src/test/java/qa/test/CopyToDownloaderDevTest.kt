package qa.test

import clients.AwsClient
import com.amazonaws.services.sqs.model.MessageAttributeValue
import com.amazonaws.services.sqs.model.SendMessageRequest
import helpers.AwsS3Helper
import helpers.AwsSqsHelper
import helpers.commonHelper
import org.testng.SkipException
import org.testng.annotations.AfterClass
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.io.File
import java.util.concurrent.atomic.AtomicInteger

const val BUCKET_NAME_Data = "as.cd.missingdoc"

@Test(groups = ["CopyToDownloaderDev"])
class CopyToDownloaderDevTest {
    private val filename by lazy { System.getProperty("test.filename")}
    private val docIds by lazy { System.getProperty("test.docIds") }
    private var docCounter = AtomicInteger()
    private val messageAttributes = mutableMapOf(
        "feedType" to MessageAttributeValue()
            .withDataType("String").withStringValue("RERUN")
    )
    private var docCount: Int? = null
    private val reportingFilePath = System.getProperty("user.dir") + "/reporting.txt"

    @DataProvider(name = "GetSources", parallel = true)
    fun getSources(): Array<List<String>> {
        println("----------------Downloading document file for from s3-----------------------")
        return try {
            val allDocs = if (filename.isNotEmpty()) {
                AwsS3Helper.getObjectAsString(BUCKET_NAME_Data, "reprocessing/$filename")
                    .split("\n")
                    .map { it.trim() }.filter { it.isNotEmpty() }
            } else if (docIds.isNotEmpty()) {
                docIds.split(",")
            } else {
                emptyList()
            }
            docCount = allDocs.size
            allDocs.distinct().withIndex().groupBy { it.index / 50 }.map { it.value.map { it.value } }.toTypedArray()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            arrayOf(listOf())
        }
    }

    @Test(dataProvider = "GetSources")
    fun reprocessDocs(documentData: List<String>) {
        val queueName = "ProcessingDevbeta1OperationalPriority1"
        try {

            documentData.forEach { docId ->
                // get list of all files from downloaded bucket
                val allFiles = AwsS3Helper.listObjects("as.docstorage.downloaded", commonHelper.getHashedS3DocumentFolderKey(docId))
                // copy files to b8 bucket
                allFiles.forEach { key ->
                    AwsClient.s3Client.copyObject("as.docstorage.downloaded", key, "as.docstorage.b8", key)
                }

                //send notification to sqs
                val pipelineRequest = """
                 {
                 "documentId": "$docId",
                 "type":"ADD_OR_UPDATE",
                 "steps":["ALL"],
                 "asIdUpdateEvent": null
                 }
                 """.trimIndent()

                println("Sending messages for doc: ${docId} counter:${docCounter.incrementAndGet()}/$docCount")
                val sqsRequest = SendMessageRequest()
                    .withMessageBody(pipelineRequest)
                    .withMessageAttributes(messageAttributes)

                AwsSqsHelper.sendMessage(queueName, sqsRequest)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @AfterClass(alwaysRun = true)
    fun teardown() {
        if (filename.isNotEmpty()) {
            val isFileExists = AwsS3Helper.doesObjectExist(BUCKET_NAME_Data, "reprocessing/$filename")
            val locationLink = "https://s3.console.aws.amazon.com/s3/buckets/as.cd.missingdoc?region=us-east-1&prefix=reprocessing/"
            if (!isFileExists) {
                File(reportingFilePath).writeText("`$filename` not found in <$locationLink|this> location. Make sure correct file name is provided")
                throw SkipException("`$filename` not found in `$BUCKET_NAME_Data/reprocessing`. Make sure correct file name is provided")
            } else if (docCount == 0) {
                File(reportingFilePath).writeText("$filename has zero document ids. Make sure file is not empty.")
                throw SkipException("`$filename` has zero document ids. Make sure file is not empty.")
            } else {
                File(reportingFilePath).writeText("$docCount documents from <$locationLink|$filename> copied to downloader-dev")
                throw SkipException("Total $docCount documents from `$filename` copied to downloader-dev")
            }
        } else if (docIds.isNotEmpty()) {
            File(reportingFilePath).writeText("Below docs are copied to downloader-dev:\n```${docIds.split(",").joinToString("\n")}``` ")
        } else if (docCount == 0) {
            File(reportingFilePath).writeText("Please provide either filename or docIds.")
            throw SkipException("Filename or docIds list is not provided")
        } else {

        }
    }
}
