package qa.test


import com.amazonaws.services.sqs.model.SendMessageRequest
import com.amazonaws.util.IOUtils
import helpers.AwsS3Helper
import helpers.AwsSqsHelper
import org.testng.annotations.AfterClass
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.time.LocalDate
import java.util.*
import java.util.concurrent.atomic.AtomicInteger


@Test(groups = ["AMPSBackfillByVendorId"])
class BackfillAMPSPOCSourcesByVendorIdTest  {
    private val startDate = "2019-01-01"
    private val endDate = "2021-09-13"
    private val sourceSummary = Collections.synchronizedList(mutableListOf<String>("<------------------------Backfilling Summary------------------------------->"))
    private val sourceExceptionSummary = Collections.synchronizedList(mutableListOf<String>("<------------------------Source Exception Summary------------------------------->"))
    private val vendorId = System.getProperty("test.text")
    private var docCounter = AtomicInteger()

    val allDates = getAllDates(startDate, endDate)

    @DataProvider(name = "GetSources", parallel = true)
    fun getSources(): Array<List<DocData>> {
        if (vendorId.isNotEmpty()){
            val sourceCode = vendorId.split(",")[0].replace("|", "-")
            println("----------------Downloading document file for $sourceCode from s3-----------------------")
            val fileStream = AwsS3Helper.readFileFromS3("harsha.test", "2021/AMPSCCSdata/$sourceCode.csv")?.objectContent
            val lines = IOUtils.toString(fileStream).split("\n").filter { it.isNotBlank() }
            val documentData = lines.map { DocData(
                sourceName = it.split(",")[0],
                fileName = it.split(",")[1],
                publishedDateTime = it.split(",")[2]
            ) }
            //val docToBackfill = documentData.filter { it.publishedDateTime.split("T")[0] in allDates }
            return documentData.withIndex().groupBy { it.index/1000 }.map { it.value.map { it.value } }.toTypedArray()
        } else {
            return arrayOf(emptyList())
        }
    }

    @AfterClass(alwaysRun = true)
    fun tearDown(){
        sourceSummary.add("${vendorId},$docCounter")
        println()
        println(sourceSummary.joinToString("\n"))
        println()
        println()
        println(sourceExceptionSummary.joinToString("\n"))
    }

    @Test(dataProvider = "GetSources")
    fun get_amps_doc_count(documentData: List<DocData>) {
        try {
            documentData.parallelStream().forEach {
                val uuid = UUID.randomUUID()
                val pipelineRequest = """
                 {
                   "Type": "Notification",
                   "MessageId": "$uuid",
                   "TopicArn": "arn:aws:sqs:us-east-1:752320408524:prod-mtnews-backfill-sqs",
                   "Subject": "Amazon S3 Notification",
                   "Message": "{\"Records\":[{\"eventVersion\":\"2.1\",\"eventSource\":\"aws:s3\",\"awsRegion\":\"us-east-1\",\"eventTime\":\"2021-09-13T08:26:04.265Z\",\"eventName\":\"ObjectCreated:Copy\",\"userIdentity\":{\"principalId\":\"AWS:AROA26KOKNPGFFYYKNLRH:ssingh@alpha-sense.com\"},\"requestParameters\":{\"sourceIPAddress\":\"103.208.71.68\"},\"responseElements\":{\"x-amz-request-id\":\"TEEGBA3GRWDZ20NT\",\"x-amz-id-2\":\"wY1mGOqiQ8fbugg091zIWv7e7trvfS65EvRTU9MkJmdLBmWXVUmyTU7Nd99yAtdJpAFCykpcoCvLEdOnEzfKouacAeNxBO9A\"},\"s3\":{\"s3SchemaVersion\":\"1.0\",\"configurationId\":\"backfill event\",\"bucket\":{\"name\":\"as.archive.feeds\",\"ownerIdentity\":{\"principalId\":\"A1876T5SETTTRC\"},\"arn\":\"arn:aws:s3:::as.archive.feeds\"},\"object\":{\"key\":\"PressRelease/${it.fileName}\",\"size\":5986,\"eTag\":\"91fc982ab4c760463c2260db42b9c161\",\"sequencer\":\"00613F0B1E7E323872\"}}}]}",
                   "Timestamp": "2021-09-13T08:26:08.191Z",
                   "SignatureVersion": "1",
                   "Signature": "NsGOzU5i+36opmNozzMonQ85MN0GtosrhmAGMXjVPtVAb4R1m4mLX+GHdJZN+5QNYFD36guB2i6b9HtFGDh2FazpeqVMHB7vbNZ7fDMDF1D2gS7xkKhQ8NnOzvdnnVvQ/VUazrMQEyYIDhXjHhmec4hQRYuTIcZZ8qJx71/2g2PlxYp7DsD9L5FEL1UfqliP+b2I++GIHCNj1TKHZalM39fJy1BOqU7T0HZjsk0HA0JMDCPPCmBQQIGIsw67GSJVuXTqua71HuQGX3RUZzEhXpnu2VLaiVQGOUDBHvcOIeCTH4bt0qeFVxi5nwkzIypj2HUov6jvMZ0FhOUa/nXEZw==",
                   "SigningCertURL": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-7ff5318490ec183fbaddaa2a969abfda.pem",
                   "UnsubscribeURL": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:752320408524:downloader-dev-mtnews-backfill:6a36c658-b37a-48e1-915c-31caff179a40"
                 }
                    """.trimIndent()

                println("Sending messages for source: $vendorId file: ${it.fileName} counter:${docCounter.incrementAndGet()}")
                val sqsRequest = SendMessageRequest()
                    .withMessageBody(pipelineRequest)
                AwsSqsHelper.sendMessage("prod-mtnews-backfill-sqs", sqsRequest)
            }
        } catch (e: Exception){
            println("------------------------------Error in $vendorId--------------------------------------------")
            e.printStackTrace()
            sourceExceptionSummary.add("$vendorId,${e.message}")
        }
    }

    private fun getAllDates(startDateStr: String, endDateStr: String): List<String> {
        val ls = mutableListOf<String>()
        var startDate = LocalDate.of(
            startDateStr.split("-")[0].toInt(),
            startDateStr.split("-")[1].toInt(),
            startDateStr.split("-")[2].toInt()
        )
        val endDate = LocalDate.of(
            endDateStr.split("-")[0].toInt(),
            endDateStr.split("-")[1].toInt(),
            endDateStr.split("-")[2].toInt()
        )
        while (startDate <= endDate) {
            ls.add(startDate.toString())
            startDate = startDate.plusDays(1)
        }
        return ls
    }
}

data class DocData(
    val sourceName: String,
    val fileName: String,
    val publishedDateTime: String
)