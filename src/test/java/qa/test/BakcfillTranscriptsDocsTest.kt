package qa.test

import com.amazonaws.util.IOUtils
import helpers.AwsS3Helper
import org.testng.annotations.AfterClass
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.security.MessageDigest
import java.util.*
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Test(groups = [ "BackfillTR"])
class BakcfillTranscriptsDocsTest {
    private val errorList = Collections.synchronizedList(mutableListOf<String>())
    private val successList = Collections.synchronizedList(mutableListOf<String>())
    private val trBucket = "as.archive.feeds.transcripts.tr"
    private val trBackfillPath = "Transcripts_prod_backfill"
    private val etBucket = "as.prod.feeds.enhanced.transcripts"
    private val ecBucket = "as.downloader.feeds.expert.calls.transcripts"
    private val downloadedBucket = "as.docstorage.downloaded"
    private val b1Bucket = "as.docstorage.b1"
    private val year = System.getProperty("test.text")


    @DataProvider(name = "docsData", parallel = true)
    fun setup(): Array<List<String>>{
        val fileStream = AwsS3Helper.readFileFromS3("harsha.test", "2021/BackfilTR/$year.csv")?.objectContent
        val lines = IOUtils.toString(fileStream).split("\n").filter { it.isNotBlank() }

        return lines.drop(1).withIndex().groupBy { it.index/100 }.map { it.value.map { it.value.trim() } }.toTypedArray()
    }

    @Test(dataProvider = "docsData")
    fun backfillECDocs(docs: List<String>){
        docs.parallelStream().forEach {docId ->
            try {
                if (docId.startsWith("EC-")){
                    //backfil EC
                    backfillEC(docId)
                } else if (docId.startsWith("TTR_")){
                    // backfill TR
                    backfillTR(docId)
                } else if (docId.startsWith("ET-")){
                    // backfill ET
                    //backfillET(docId)
                } else {
                    errorList.add(docId)
                }
            } catch (e: Exception){
                errorList.add(docId)
                e.printStackTrace()
            }
        }
    }

    private fun backfillEC(docId: String){
        try {
            // Get latest version of EC doc from S3
            val fromPathRealtime = "realtime/${docId.replace("EC","INT")}"
            val fromPathBackfill = "backfill/${docId.replace("EC","INT")}"
            var ls = AwsS3Helper.listObjects(ecBucket, fromPathBackfill)
                .sortedByDescending { AwsS3Helper.getLastModified(ecBucket, it) }
            if (ls.isEmpty()){
                ls = AwsS3Helper.listObjects(ecBucket, fromPathRealtime)
                    .sortedByDescending { AwsS3Helper.getLastModified(ecBucket, it) }
            }

            AwsS3Helper.copyObject(ecBucket, ls.first(),
                ecBucket, ls.first().replace("backfill","backfill/reUpload").replace("realtime","backfill/reUpload"))
            println("backfilled $docId")
            successList.add("$docId,${ls.size},${ls.first()}")
        } catch (e: Exception){
            errorList.add(docId)
        }
    }

    private fun backfillTR(docId: String){
        try {
            // get latest version of TR from s3
            val prefix = "Transcripts/${docId.split("_")[1]}"
            val listedFiles = AwsS3Helper.listObjects(trBucket, prefix).sortedByDescending { AwsS3Helper.getLastModified(trBucket, it) }
            if (listedFiles.isNotEmpty()){
                val latestVersion = listedFiles.first()
                println("found latest version of $docId is $latestVersion")
                //reupload this file to s3
                AwsS3Helper.copyObject(trBucket, latestVersion, trBucket, latestVersion.replace("Transcripts", trBackfillPath))
                println("backfilled $docId")
            } else {
                println("Working on creating zip for $docId")
                val zipFileName = docId.split("_")[1] + "_T-00000"
                val fileName = docId.split("_")[1] + "_T.xml"
                val filePath = createTempFileFromS3Content(downloadedBucket,getHashedS3DocumentFolderKey(docId) + "/Downloaded/$docId.xml", fileName)
                val zipFilePath = createZipFileFromGivenFiles(listOf(filePath), zipFileName)
                uploadZipFiletoS3(trBucket, "$trBackfillPath/$zipFileName.zip", zipFilePath)
                File(filePath).delete()
                File(zipFilePath).delete()
            }
            successList.add(docId)
        } catch (e: Exception){
            errorList.add(docId)
        }
    }

    @AfterClass(alwaysRun = true)
    fun teardown(){
        val path1 = System.getProperty("user.dir") + "/errorFiles_${year}_${Date().time}.csv"
        File(path1).writeText(errorList.map { "\n$it" }.toString().replace(",",""))
        AwsS3Helper.uploadFile("harsha.test", "2021/BackfilTR/errorFiles_${year}.csv", File(path1))

        val path2 = System.getProperty("user.dir") + "/successFiles_${year}_${Date().time}.csv"
        File(path2).writeText(successList.map { "\n$it" }.toString().replace(",",""))
        AwsS3Helper.uploadFile("harsha.test", "2021/BackfilTR/successFiles_${year}.csv", File(path2))
    }

    private fun createTempFileFromS3Content(bucket:String, key:String, fileName:String):String{
        println("creating temp file $fileName")
        val s3File = AwsS3Helper.readFileFromS3(bucket, key)?.objectContent
        val content = IOUtils.toString(s3File)
        val sourceFile = System.getProperty("user.dir") + "/$fileName"
        File(sourceFile).writeText(content)
        return sourceFile
    }

    private fun createZipFileFromGivenFiles(filePaths:List<String>, fileName:String): String{
        println("creating temp zip file $fileName")
        val zipFilePath = System.getProperty("user.dir") + "/$fileName.zip"
        val fos = FileOutputStream(zipFilePath)
        val zipOut = ZipOutputStream(fos)
        filePaths.forEach { sourceFile ->
            val fileToZip = File(sourceFile)
            val fis = FileInputStream(fileToZip)
            val zipEntry = ZipEntry(fileToZip.name)
            zipOut.putNextEntry(zipEntry)
            val bytes = ByteArray(1024)
            var length: Int
            while (fis.read(bytes).also { length = it } >= 0) {
                zipOut.write(bytes, 0, length)
            }
            fis.close()
        }
        zipOut.close()
        fos.close()
        return zipFilePath
    }

    private fun uploadZipFiletoS3(bucket:String, key: String, zipFilePath:String){
        println("uploading zip file $zipFilePath")
        AwsS3Helper.uploadFile(bucket, key, File(zipFilePath))
    }

    private fun getMd5HashOfString(text: String): String {
        val md = MessageDigest.getInstance("MD5")
        val md5HashBytes = md.digest(text.toByteArray()).toTypedArray()
        val md5OfDocId = StringBuilder(md5HashBytes.size * 2)
        md.digest(text.toByteArray()).forEach { byte ->
            md5OfDocId.append(String.format("%2X", byte).replace(" ", "0"))
        }
        return md5OfDocId.toString().toLowerCase()
    }

    private fun getHashedS3DocumentFolderKey(docId: String): String {
        return "${getMd5HashOfString(docId).substring(0, 5)}.$docId"
    }
}