package qa.test

import common.SourceIds
import dynamodb.BackfillDynamoRecords
import dynamodb.ExchangeFeedsDynamo
import helpers.AwsS3Helper
import helpers.TableGenerator
import org.testng.annotations.Test
import steps.BackfillManagerSteps
import java.io.File
import java.time.LocalDate
import java.util.*

@Test(groups = ["BackfillExchange"])
class BackfillExchangeTest {
    private val exchangeBackfillDynamo = ExchangeFeedsDynamo()
    private val bmSteps = BackfillManagerSteps()
    private val backfilledRecords = mutableListOf<BackfillDynamoRecords>()

    private val reportingFilePath = System.getProperty("user.dir") + "/reporting.txt"
    private val batchCount by lazy { bmSteps.getBatchCount() }
    private val tableGenerator = TableGenerator()

    @Test
    fun backfillExchange() {
        val exchanges = bmSteps.getExchangePriority()

        println(batchCount)
        println(bmSteps.messageToReport)

        exchanges.forEach { exchange ->
            println("Backfilling exchange: ${exchange.exchange}")
            if (backfilledRecords.size < batchCount.numberOfDocsCanBeBackfilled) {
                val numberOfDocsToFetchFromDynamo = batchCount.numberOfDocsCanBeBackfilled - backfilledRecords.size
                backfillDocumentsForGivenExchange(exchange.exchange, numberOfDocsToFetchFromDynamo)
            }
            println("------------------------------------------------------------")
        }

        val backfilledSummary = backfilledRecords.groupBy { it.vendorName }.map { it.key to it.value.size }

        val s3KeyToUploadDocList = "2024/BackfillExchanges/data/${LocalDate.now()}/backfilledRecords_${Date().time}.csv"
        //upload backfilled records to s3
        if (backfilledRecords.isNotEmpty()) {
            val s3FileLink = "https://us-east-1.console.aws.amazon.com/s3/object/${bmSteps.auditBucket}?region=us-east-1&bucketType=general&prefix=${s3KeyToUploadDocList}"

            val auditFilePath = System.getProperty("user.dir") + "/backfilledRecords_${Date().time}.csv"
            File(auditFilePath).writeText(backfilledRecords.joinToString("\n") { "${it.id},${it.vendorName}" })
            AwsS3Helper.uploadFile(bmSteps.auditBucket, s3KeyToUploadDocList, File(auditFilePath))
            File(auditFilePath).deleteOnExit()
            val backfilledSummaryMessage = tableGenerator.generateTable(listOf("Vendor Name", "#Docs"), backfilledSummary.map { listOf(SourceIds.sourceIdVendorMapping[it.first] ?: it.first!!, it.second.toString()) })
            bmSteps.messageToReport.append("\n*Backfilling Summary*: ```$backfilledSummaryMessage```\n")
            bmSteps.messageToReport.append("Backfilled docIds uploaded <$s3FileLink|here>")
        } else if (batchCount.numberOfDocsCanBeBackfilled != 0) {
            bmSteps.messageToReport.append("\n```Found no docs to backfill in dynamo```")
        }

        //send slack message
        println(bmSteps.messageToReport)
        File(reportingFilePath).writeText(bmSteps.messageToReport.toString())
    }

    private fun backfillDocumentsForGivenExchange(exchange: String?, limit: Int) {
        do {
            val allDocsToBackfill = exchangeBackfillDynamo.getNotBackfilledDocs(exchange, limit)
            println("found ${allDocsToBackfill.size} records to backfill from dynamo")
            if (allDocsToBackfill.isNotEmpty()) {
                val batchSize = 4000

                allDocsToBackfill.chunked(batchSize).forEach { batch ->
                    bmSteps.sendDocsToPipelineQueue(batchCount, batch)
                    exchangeBackfillDynamo.updateDynamoRecordsInBatch(batch)
                    Thread.sleep(5000)
                }

                println("Updated dynamo records with ttl and isBackfilled")
            } else {
                println("All docId from dynamo are backfilled for $exchange")
            }

            backfilledRecords.addAll(allDocsToBackfill)
        } while (backfilledRecords.size < batchCount.numberOfDocsCanBeBackfilled && allDocsToBackfill.isNotEmpty())
    }
}
