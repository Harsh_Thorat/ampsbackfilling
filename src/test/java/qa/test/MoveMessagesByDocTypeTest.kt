package qa.test

import clients.AwsClient
import com.amazonaws.services.sqs.model.SendMessageBatchRequest
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry
import helpers.AwsSqsHelper
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.io.File
import java.util.*

@Test(groups = ["DeleteMessagesByDocType"])
class MoveMessagesByDocTypeTest  {
    private var limit = 50000
    private val fromQueue = "PipelineSmallProdgamma4DeadLetterQueue"
    private val toQueue = "PipelineSmallProdgamma4Backfill"
    private val prefixToIgnore = "MR-MRDC"
    private val deleteddocidList = mutableListOf<String>()
    private val notDeleteddocidList = mutableListOf<String>()

    @Test
    fun moveMessages() {
        val fromQueueSize = AwsSqsHelper.getQueueSize(fromQueue)
        limit = if (fromQueueSize < limit) fromQueueSize.toInt() else limit

        val batchLimit = 10
        (1..limit / batchLimit)
            .toList()
            .parallelStream()
            .forEach {
                try {
                    val msgs = AwsSqsHelper.getMessages(fromQueue, 10)

                    val messagesToNotDeleted = msgs.filter { !it.body.contains(prefixToIgnore) }
                    notDeleteddocidList.addAll(messagesToNotDeleted.map { it.body.split("\n")[4].split("accessionNumber").last() })

                    val messagesToBeDeleted = msgs.filter { it.body.contains(prefixToIgnore) }
                    deleteddocidList.addAll(messagesToBeDeleted.map { it.body.split("\n")[4].split("accessionNumber").last() })

                    if (messagesToNotDeleted.isNotEmpty()){
                        val batchRequestEntries = messagesToNotDeleted.map { doc ->
                            SendMessageBatchRequestEntry(UUID.randomUUID().toString(), doc.body)
                                .withMessageAttributes(doc.messageAttributes)
                        }

                        val sendBatchRequest = SendMessageBatchRequest()
                            .withQueueUrl(toQueue)
                            .withEntries(batchRequestEntries)

                        AwsClient.sqsClient.sendMessageBatch(sendBatchRequest)
                    }

                    AwsSqsHelper.deleteMessages(fromQueue, msgs)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        File("deleteddocidList_${Date().time}.txt").writeText(deleteddocidList.joinToString("\n"))
        File("notdeleteddocidList_${Date().time}.txt").writeText(notDeleteddocidList.joinToString("\n"))
    }
}