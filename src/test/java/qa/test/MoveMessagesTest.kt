package qa.test


import clients.AwsClient
import com.amazonaws.services.sqs.model.SendMessageBatchRequest
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry
import helpers.AwsSqsHelper
import org.testng.annotations.Test
import java.util.*

@Test(groups = ["MoveMessages"])
class MoveMessagesTest {
    var limit = 10000
    val fromQueue = "PipelineSmallProdgamma4Backfill"
    val toQueue = "PipelineLargeProdgamma4Backfill"

    @Test
    fun moveMessages() {
        val fromQueueSize = AwsSqsHelper.getQueueSize(fromQueue)
        limit = if (fromQueueSize < limit) fromQueueSize.toInt() else limit

        val batchLimit = 10
        (1..limit / batchLimit)
            .toList()
            .parallelStream().forEach {
                try {
                    val msgs = AwsSqsHelper.getMessages(fromQueue, 10)

                    val batchRequestEntries = msgs.map { doc ->
                        SendMessageBatchRequestEntry(UUID.randomUUID().toString(), doc.body)
                            .withMessageAttributes(doc.messageAttributes)
                    }

                    val sendBatchRequest = SendMessageBatchRequest()
                        .withQueueUrl(toQueue)
                        .withEntries(batchRequestEntries)

                    AwsClient.sqsClient.sendMessageBatch(sendBatchRequest)
                    AwsSqsHelper.deleteMessages(fromQueue, msgs)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
    }
}