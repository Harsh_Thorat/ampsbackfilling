package qa.test

import com.amazonaws.services.sqs.model.MessageAttributeValue
import com.amazonaws.services.sqs.model.SendMessageRequest
import helpers.AwsS3Helper
import helpers.AwsSqsHelper
import org.testng.SkipException
import org.testng.annotations.AfterClass
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.io.File
import java.util.concurrent.atomic.AtomicInteger

const val BUCKET_NAME = "as.cd.missingdoc"

@Test(groups = ["ReprocessDocuments"])
class ReprocessDocsTest {
    private val filename by lazy { System.getProperty("test.filename")}
    private val docIds by lazy { System.getProperty("test.docIds") }
    private val queuePriority: String by lazy { System.getProperty("test.queuePriority").trim()}
    private val env: String by lazy { System.getProperty("test.env")}
    private val stepsArgs by lazy { System.getProperty("test.steps").split(",") }
    private val stepsList by lazy { stepsArgs.joinToString("\",\"", "\"", "\"") }
    private var docCounter = AtomicInteger()
    private val messageAttributes = mutableMapOf(
        "feedType" to MessageAttributeValue()
            .withDataType("String").withStringValue("RERUN")
    )
    private var docCount: Int? = null
    private val queueName by lazy { getQueueName(env, queuePriority) }
    private val reportingFilePath = System.getProperty("user.dir") + "/reporting.txt"

    @DataProvider(name = "GetSources", parallel = true)
    fun getSources(): Array<List<String>> {
        println("----------------Downloading document file for from s3-----------------------")
        return try {
            val allDocs = if (filename.isNotEmpty()) {
                AwsS3Helper.getObjectAsString(BUCKET_NAME, "reprocessing/$filename")
                    .split("\n")
                    .map { it.trim() }.filter { it.isNotEmpty() }
            } else if (docIds.isNotEmpty()) {
                docIds.split(",")
            } else {
                emptyList()
            }
            docCount = allDocs.size
            allDocs.distinct().withIndex().groupBy { it.index / 50 }.map { it.value.map { it.value } }.toTypedArray()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            arrayOf(listOf())
        }
    }

    @Test(dataProvider = "GetSources")
    fun reprocessDocs(documentData: List<String>) {
        try {
            documentData.forEach { docId ->
                val pipelineRequest = """
                 {
                 "documentId": "$docId",
                 "type":"ADD_OR_UPDATE",
                 "steps":[$stepsList],
                 "asIdUpdateEvent": null
                 }
                 """.trimIndent()

                println("Sending messages for doc: ${docId} counter:${docCounter.incrementAndGet()}/$docCount")
                val sqsRequest = SendMessageRequest()
                    .withMessageBody(pipelineRequest)
                    .withMessageAttributes(messageAttributes)

                AwsSqsHelper.sendMessage(queueName, sqsRequest)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @AfterClass(alwaysRun = true)
    fun teardown() {
        if (filename.isNotEmpty()) {
            val isFileExists = AwsS3Helper.doesObjectExist(BUCKET_NAME, "reprocessing/$filename")
            val locationLink = "https://s3.console.aws.amazon.com/s3/buckets/as.cd.missingdoc?region=us-east-1&prefix=reprocessing/"
            if (!isFileExists) {
                File(reportingFilePath).writeText("`$filename` not found in <$locationLink|this> location. Make sure correct file name is provided")
                throw SkipException("`$filename` not found in `$BUCKET_NAME/reprocessing`. Make sure correct file name is provided")
            } else if (docCount == 0) {
                File(reportingFilePath).writeText("$filename has zero document ids. Make sure file is not empty.")
                throw SkipException("`$filename` has zero document ids. Make sure file is not empty.")
            } else {
                File(reportingFilePath).writeText("$docCount documents from <$locationLink|$filename> sent to `$queueName`")
                throw SkipException("Total $docCount documents from `$filename` sent to `$queueName` with steps `$stepsList`")
            }
        } else if (docIds.isNotEmpty()) {
            File(reportingFilePath).writeText("Below docs are sent to `$queueName`:\n```${docIds.split(",").joinToString("\n")}``` ")
        } else if (docCount == 0) {
            File(reportingFilePath).writeText("Please provide either filename or docIds.")
            throw SkipException("Filename or docIds list is not provided")
        } else {

        }
    }

    private fun getQueueName(env: String, queuePriority: String): String {
        val mapQueue = mapOf("dev" to "Devbeta1", "rc" to "Stagingdelta1", "prod" to "Prodgamma4")
        return "Processing${mapQueue[env]}Operational$queuePriority"
    }
}
