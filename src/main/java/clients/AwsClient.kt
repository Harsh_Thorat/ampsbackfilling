package clients

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.regions.Regions
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.sqs.AmazonSQSAsync
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder

object AwsClient{
    private val clientRegion = Regions.US_EAST_1
    val s3Client = AmazonS3ClientBuilder.standard()
            .withRegion(clientRegion)
            .withCredentials(AWSStaticCredentialsProvider(DefaultAWSCredentialsProviderChain().credentials))
            .build()

    val dynamoClient = AmazonDynamoDBAsyncClientBuilder.standard()
            .withRegion(clientRegion)
            .withCredentials(AWSStaticCredentialsProvider(DefaultAWSCredentialsProviderChain().credentials))
            .build()

    val sqsClient: AmazonSQSAsync by lazy {
        AmazonSQSAsyncClientBuilder.standard()
            .withRegion(clientRegion)
            .withCredentials(AWSStaticCredentialsProvider(DefaultAWSCredentialsProviderChain().credentials))
            .build()
    }
}