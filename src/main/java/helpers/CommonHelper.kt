package helpers

import java.security.MessageDigest

object commonHelper {

    fun getHashedS3DocumentFolderKey(text: String): String {
        val md = MessageDigest.getInstance("MD5")
        val md5HashBytes = md.digest(text.toByteArray()).toTypedArray()
        val md5OfDocId = StringBuilder(md5HashBytes.size * 2)
        md.digest(text.toByteArray()).forEach { byte ->
            md5OfDocId.append(String.format("%2X", byte).replace(" ", "0"))
        }
        return md5OfDocId.toString().toLowerCase().substring(0,5) + ".$text"
    }
}