package helpers

class TableGenerator {
    private val PADDING_SIZE = 1
    private val NEW_LINE = "\n"
    private val TABLE_JOINT_SYMBOL = "+"
    private val TABLE_V_SPLIT_SYMBOL = "|"
    private val TABLE_H_SPLIT_SYMBOL = "-"

    fun generateTable(
        headersList: List<String>,
        rowsList: List<List<String>>,
        vararg overRiddenHeaderHeight: Int
    ): String {
        val stringBuilder = StringBuilder()
        val rowHeight = if (overRiddenHeaderHeight.isNotEmpty()) overRiddenHeaderHeight[0] else 1
        val columnMaxWidthMapping = getMaximumWidthOfTable(headersList, rowsList)
        stringBuilder.append(NEW_LINE)
        createRowLine(stringBuilder, headersList.size, columnMaxWidthMapping)
        stringBuilder.append(NEW_LINE)
        for (headerIndex in headersList.indices) {
            fillCell(stringBuilder, headersList[headerIndex], headerIndex, columnMaxWidthMapping)
        }
        stringBuilder.append(NEW_LINE)
        createRowLine(stringBuilder, headersList.size, columnMaxWidthMapping)
        for (row in rowsList) {
            for (i in 0 until rowHeight) {
                stringBuilder.append(NEW_LINE)
            }
            for (cellIndex in row.indices) {
                fillCell(stringBuilder, row[cellIndex], cellIndex, columnMaxWidthMapping)
            }
        }
        stringBuilder.append(NEW_LINE)
        createRowLine(stringBuilder, headersList.size, columnMaxWidthMapping)
        stringBuilder.append(NEW_LINE)
        return stringBuilder.toString()
    }

    private fun fillSpace(stringBuilder: StringBuilder, length: Int) {
        for (i in 0 until length) {
            stringBuilder.append(" ")
        }
    }

    private fun createRowLine(
        stringBuilder: StringBuilder,
        headersListSize: Int,
        columnMaxWidthMapping: Map<Int, Int>
    ) {
        for (i in 0 until headersListSize) {
            if (i == 0) {
                stringBuilder.append(TABLE_JOINT_SYMBOL)
            }
            for (j in 0 until (columnMaxWidthMapping[i] ?: 0) + PADDING_SIZE * 2) {
                stringBuilder.append(TABLE_H_SPLIT_SYMBOL)
            }
            stringBuilder.append(TABLE_JOINT_SYMBOL)
        }
    }

    private fun getMaximumWidthOfTable(headersList: List<String>, rowsList: List<List<String?>>): Map<Int, Int> {
        val columnMaxWidthMapping: MutableMap<Int, Int> = hashMapOf()

        // Initialize column widths based on headers
        headersList.forEachIndexed { index, header ->
            columnMaxWidthMapping[index] = header.length
        }

        // Iterate over rows to adjust column widths based on cell content
        rowsList.forEach { row ->
            row.forEachIndexed { index, cell ->
                if (cell != null && cell.length > (columnMaxWidthMapping[index] ?: 0)) {
                    columnMaxWidthMapping[index] = cell.length
                }
            }
        }

        // Adjust column widths to be even (optional)
        columnMaxWidthMapping.keys.forEach { key ->
            if (columnMaxWidthMapping[key]!! % 2 != 0) {
                columnMaxWidthMapping[key] = columnMaxWidthMapping[key]!! + 1
            }
        }

        return columnMaxWidthMapping
    }

    private fun getOptimumCellPadding(
        cellIndex: Int,
        dataLength: Int,
        columnMaxWidthMapping: Map<Int, Int>,
        cellPaddingSize: Int
    ): Int {
        var adjustedDataLength = dataLength
        var adjustedCellPaddingSize = cellPaddingSize
        if (adjustedDataLength % 2 != 0) {
            adjustedDataLength++
        }
        if (adjustedDataLength < columnMaxWidthMapping[cellIndex]!!) {
            adjustedCellPaddingSize += (columnMaxWidthMapping[cellIndex]!! - adjustedDataLength) / 2
        }
        return adjustedCellPaddingSize
    }

    private fun fillCell(
        stringBuilder: StringBuilder,
        cell: String?,
        cellIndex: Int,
        columnMaxWidthMapping: Map<Int, Int>
    ) {
        if (cell == null) {
            println("Null cell encountered at index: $cellIndex")
            return
        }
        val cellPaddingSize = getOptimumCellPadding(cellIndex, cell.length, columnMaxWidthMapping, PADDING_SIZE)
        if (cellIndex == 0) {
            stringBuilder.append(TABLE_V_SPLIT_SYMBOL)
        }
        fillSpace(stringBuilder, cellPaddingSize)
        stringBuilder.append(cell)
        if (cell.length % 2 != 0) {
            stringBuilder.append(" ")
        }
        fillSpace(stringBuilder, cellPaddingSize)
        stringBuilder.append(TABLE_V_SPLIT_SYMBOL)
    }


/*    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val tableGenerator = TableGenerator()
            val headersList: MutableList<String> = ArrayList()
            headersList.add("Id")
            headersList.add("F-Name")
            headersList.add("L-Name")
            headersList.add("Email")
            val rowsList: MutableList<List<String>> = ArrayList()
            for (i in 0..4) {
                val row: MutableList<String> = ArrayList()
                row.add(UUID.randomUUID().toString())
                row.add(UUID.randomUUID().toString())
                row.add(UUID.randomUUID().toString())
                row.add(UUID.randomUUID().toString())
                rowsList.add(row)
            }
            println(tableGenerator.generateTable(headersList, rowsList))
        }
    }*/
}