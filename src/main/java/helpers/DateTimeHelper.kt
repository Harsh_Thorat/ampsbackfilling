package helpers

import java.time.LocalDate
import java.time.LocalDateTime
import java.util.TimeZone

object DateTimeHelper {
    val usCurrentTime = LocalDateTime.now(TimeZone.getTimeZone("America/New_York").toZoneId())
    fun isUsBusinessHour(): Boolean {
        val currentHour = usCurrentTime.hour
        return currentHour in 9..17
    }

    fun isWeekend():Boolean {
        val dayOfWeek = usCurrentTime.dayOfWeek.value
        return dayOfWeek == 6 || dayOfWeek == 7
    }

    fun isWeekday():Boolean {
        val dayOfWeek = usCurrentTime.dayOfWeek.value
        return dayOfWeek != 6 && dayOfWeek != 7
    }
}