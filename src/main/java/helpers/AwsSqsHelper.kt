package helpers

import clients.AwsClient.sqsClient
import com.amazonaws.ClientConfiguration
import com.amazonaws.services.sqs.model.*
import org.testng.SkipException
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue

private const val SQS_SIZE_THRESHOLD = 30

object AwsSqsHelper {

    fun listQueues(): List<String> {
        return sqsClient.listQueues().queueUrls
    }

    fun doesQueueExists(queueName: String): Boolean {
        return listQueues().contains(getQueueUrl(queueName))
    }

    fun sendMessage(queueName: String, request: SendMessageRequest): String {
        request.queueUrl = getQueueUrl(queueName)
        //println("Sending message [$request]")
        return sqsClient.sendMessage(request).messageId
    }

    fun sendMessages(queueName: String, vararg requests: SendMessageBatchRequestEntry): List<SendMessageBatchResult> {
        val results = ConcurrentLinkedQueue<SendMessageBatchResult>()
        val queueUrl = getQueueUrl(queueName)
        requests.toList()
            .chunked(10)
            .chunked(ClientConfiguration.DEFAULT_MAX_CONNECTIONS) // given it's not overridden during client init
            .parallelStream()
            .forEach { chunk ->
                val chunkId = UUID.randomUUID()
                chunk.forEachIndexed { i, batchEntities ->
                    println("[Chunk:$chunkId-batch_$i] Sending batch with ${batchEntities.size} entities")
                    results.add(sqsClient.sendMessageBatch(queueUrl, batchEntities.mapIndexed { idIndex, it ->
                        it.withId("$chunkId-batch_$i-idIndex_$idIndex")
                    }))
                }
            }
        return results.toList()
    }

    fun getMessages(queueName: String, limit: Int = 10, waitTimeSeconds: Int = 0): List<Message> {
        val queueUrl = getQueueUrl(queueName)
        println("Receiving messages from [$queueName]")
        return sqsClient.receiveMessage(
            ReceiveMessageRequest()
                .withWaitTimeSeconds(waitTimeSeconds)
                .withMaxNumberOfMessages(limit)
                .withQueueUrl(queueUrl)
        ).messages
    }

    fun getMessagesByUrl(queueUrl: String, limit: Int, waitTimeSeconds: Int = 0): List<Message> {
        println("Receiving messages from [$queueUrl]")
        return sqsClient.receiveMessage(
            ReceiveMessageRequest()
                .withWaitTimeSeconds(waitTimeSeconds)
                .withMaxNumberOfMessages(limit)
                .withQueueUrl(queueUrl)
        )
            .messages
    }

    fun getAllMessages(queueName: String): MutableList<Message> {
        val totalMessages = mutableListOf<Message>()
        var messages = getMessages(queueName, 1, 5)
        while (messages.isNotEmpty()) {
            totalMessages.addAll(messages)
            deleteMessages(queueName, messages)
            messages = getMessages(queueName, 5)
        }
        return totalMessages.toMutableList()
    }

    fun getQueueSize(queueName: String): Long {
        val size = sqsClient.getQueueAttributes(
            queueName,
            listOf("ApproximateNumberOfMessages")
        ).attributes["ApproximateNumberOfMessages"]!!.toLong()
        return size
    }

    fun getInFlightSize(queueName: String): Long {
        return sqsClient.getQueueAttributes(
            queueName,
            listOf("ApproximateNumberOfMessagesNotVisible")
        ).attributes["ApproximateNumberOfMessagesNotVisible"]!!.toLong()
    }

    fun deleteMessages(queueName: String, messages: List<Message>) {
        val queueUrl = getQueueUrl(queueName)
        println("Deleting ${messages.size} messages from [$queueName]: [$messages]")
        sqsClient.deleteMessageBatch(
            DeleteMessageBatchRequest(queueUrl, messages
                .mapIndexed { index, message ->
                    DeleteMessageBatchRequestEntry(index.toString(), message.receiptHandle)
                })
        )
    }

    fun deleteMessagesByUrl(queueUrl: String, messages: List<Message>) {
        println("Deleting ${messages.size} messages from [$queueUrl]: [$messages]")
        sqsClient.deleteMessageBatch(
            DeleteMessageBatchRequest(queueUrl, messages
                .mapIndexed { index, message ->
                    DeleteMessageBatchRequestEntry(index.toString(), message.receiptHandle)
                })
        )
    }

    fun deleteAllMessages(queueName: String) {
        var messages = getMessages(queueName, 5)
        while (messages.isNotEmpty()) {
            deleteMessages(queueName, messages)
            messages = (getMessages(queueName, 5))
        }
    }

    fun deleteAllMessagesByUrl(queueUrl: String) {
        var messages = getMessagesByUrl(queueUrl, 10)
        while (messages.isNotEmpty()) {
            deleteMessagesByUrl(queueUrl, messages)
            messages = (getMessagesByUrl(queueUrl, 10))
        }
    }

    fun purgeQueue(queueName: String) {
        val queueUrl = getQueueUrl(queueName)
        println("Purging queue [$queueName]")
        sqsClient.purgeQueue(PurgeQueueRequest(queueUrl))
    }

    fun purgeQueueByUrl(queueUrl: String) {
        println("Purging queue [$queueUrl]")
        sqsClient.purgeQueue(PurgeQueueRequest(queueUrl))
    }

    fun isSqsOverfilled(queueName: String, sqsSizeThreshold: Int = SQS_SIZE_THRESHOLD): Boolean {
        return getQueueSize(queueName) > sqsSizeThreshold
    }

    fun skipTestIfSqsAreOverfilled(vararg sqsList: String, sqsSizeThreshold: Int = SQS_SIZE_THRESHOLD) {
        sqsList.asList().parallelStream().forEach {
            println("Check if $it sqs is overfilled")
            if (isSqsOverfilled(it, sqsSizeThreshold)) {
                println("Sqs $it is overfilled. Test cannot be executed.")
                throw SkipException("Sqs $it is overfilled. Test cannot be executed.")
            }
        }
    }

    private fun getQueueUrl(queueName: String): String {
        val getQueueUrlRequest = GetQueueUrlRequest(queueName)
        return sqsClient.getQueueUrl(getQueueUrlRequest).queueUrl
    }
}