package helpers


import clients.AwsClient.s3Client
import com.amazonaws.services.s3.model.ListObjectsRequest
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.services.s3.model.S3Object
import com.google.gson.Gson
import java.io.ByteArrayInputStream
import java.io.File
import java.io.InputStream
import java.util.*
import java.util.concurrent.TimeUnit

object AwsS3Helper {

    private const val AWS_PATH = "https://s3.console.aws.amazon.com/s3/object/{}/{}?region=us-east-1&tab=overview"
    private val gson = Gson()


    /**
     * Actions
     */

    fun listFolders(bucket: String, prefix: String): List<String> {
        return s3Client.listObjects(
            ListObjectsRequest()
                .withBucketName(bucket)
                .withPrefix(prefix)
                .withDelimiter("/")
        )
            .commonPrefixes
    }

    fun listObjects(bucket: String, prefix: String, suffix: String = "", paginate: Boolean = true): List<String> {
        var listing = s3Client.listObjects(
            ListObjectsRequest()
                .withBucketName(bucket)
                .withPrefix(prefix)
        )

        val keys = ArrayList<String>()

        keys.addAll(listing.objectSummaries
            .asSequence()
            .filter { !it.key.endsWith("/") } // getObjects
            .filter { it.key.endsWith(suffix) }
            .map { it.key }
        )

        while (listing.isTruncated && paginate) {
            listing = s3Client.listNextBatchOfObjects(listing)
            keys.addAll(listing.objectSummaries
                .asSequence()
                .filter { !it.key.endsWith("/") } // getObjects
                .filter { it.key.endsWith(suffix) }
                .map { it.key }
            )
        }
        return keys
    }

    fun listBuckets(): List<String> {
        return s3Client.listBuckets().map { it.name }
    }

    fun getObjectLength(bucket: String, key: String): Long {
        return s3Client.getObjectMetadata(bucket, key).contentLength
    }

    fun getLastModified(bucket: String, key: String): Date {
        return s3Client.getObjectMetadata(bucket, key).lastModified
    }

    fun getObjectAsString(bucket: String, key: String): String {
        println("Getting s3 file from $bucket bucket and $key key")
        return s3Client.getObjectAsString(bucket, key)
    }

    fun getDataFromS3JsonAsObject(bucket: String, key: String, classOfT: Class<out Any>): Any? {
        return gson.fromJson(s3Client.getObjectAsString(bucket, key), classOfT)
    }

    fun deleteObject(bucket: String, key: String) {
        s3Client.deleteObject(bucket, key)
    }

    fun readFileFromS3(s3BucketName: String, s3Key: String): S3Object? {
        return s3Client.getObject(s3BucketName, s3Key)
    }

    fun getFileSizeFromSs3(s3BucketName: String, s3Key: String): Long? {
        return s3Client.listObjects(s3BucketName, s3Key).objectSummaries[0].size
    }

    fun getAllFilesFromGivenS3Path(s3Bucket: String, s3Key: String): List<String> {
        val allS3Files = mutableListOf<String>()
        s3Client.listObjects(s3Bucket, s3Key).objectSummaries.mapTo(allS3Files) { file ->
            file.key
        }
        return allS3Files
    }

    fun uploadFile(bucket: String, key: String, file: File) {
        s3Client.putObject(bucket, key, file)
    }

    fun copyObject(fromBucket: String, keyFrom: String, toBucket: String, toKey: String) {
        s3Client.copyObject(fromBucket, keyFrom, toBucket, toKey)
    }


    fun uploadFile(bucket: String, key: String, fileStream: InputStream, contentLength: Long) {
        val objectMeta = ObjectMetadata()
        objectMeta.serverSideEncryption = ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION
        objectMeta.contentLength = contentLength
        val putObjReq = PutObjectRequest(bucket, key, fileStream, objectMeta)
        s3Client.putObject(putObjReq)
    }

    /**
     * Checks
     */

    fun doesObjectExist(bucket: String, key: String): Boolean {
        return s3Client.doesObjectExist(bucket, key)
    }
}