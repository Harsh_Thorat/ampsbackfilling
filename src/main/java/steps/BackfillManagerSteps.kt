package steps

import clients.AwsClient
import com.amazonaws.services.sqs.model.MessageAttributeValue
import com.amazonaws.services.sqs.model.SendMessageBatchRequest
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry
import common.SourceIds
import dynamodb.BackfillDynamoRecords
import helpers.AwsS3Helper
import helpers.AwsSqsHelper
import helpers.DateTimeHelper
import java.util.*

class BackfillManagerSteps {
    val messageToReport = StringBuilder()

    private val priorityFileS3Key = "2024/BackfillExchanges/ExchangePriority.csv"
    val auditBucket = "as.cd.missingdoc"

    private val backfillQueueNameToSendMessages = "ProcessingProdgamma4Backfill"
    private val smallBackfillQueue = "PipelineSmallProdgamma4Backfill"
    private val largeBackfillQueue = "PipelineLargeProdgamma4Backfill"
    private val backfillPreprocessorQueue = "ProcessingProdgamma4Backfill"
    private val operationalP1Queue = "ProcessingProdgamma4OperationalPriority1"
    private val operationalP2Queue = "ProcessingProdgamma4OperationalPriority2"
    private val indexingQueue = "prod_platform-search_sqi_public_docs_low_priority"

    fun getExchangePriority(): List<ExchangesData> {
        val exchangeInputParam = System.getProperty("test.exchange") ?: ""
        val startedByUser = System.getProperty("test.startedByUser")
        if (!exchangeInputParam.isNullOrBlank()) {
            messageToReport.append("Backfill initiated by `$startedByUser` for exchange `${SourceIds.sourceIdVendorMapping[exchangeInputParam]?:exchangeInputParam}`\n\n")
            return listOf(ExchangesData(exchange = exchangeInputParam, priority = 1))
        }

        // read file
        val data = AwsS3Helper.getObjectAsString(auditBucket, priorityFileS3Key)
        //val data = "SEDAR-1,1\nSEDAR-2,2\nSEDAR-3,3"
        val exchanges = data.split("\n").map {
            val exchangeData = it.split(",")
            ExchangesData(exchange = exchangeData[0], priority = exchangeData[1].toInt())
        }.toMutableList()
        exchanges.add(ExchangesData())
        return exchanges.sortedBy { it.priority }
    }

    fun getBatchCount(): ProcessingData {
        println("Day of the week is ${DateTimeHelper.usCurrentTime.dayOfWeek} and time is ${DateTimeHelper.usCurrentTime}. isBusinessHour:${DateTimeHelper.isUsBusinessHour()}")
        val numberOfDocsCanSentToBackfillQueue = getNumberOfDocsCanBeProcessedWithBackfillQueue()
        if (numberOfDocsCanSentToBackfillQueue > 0){
            return ProcessingData(numberOfDocsCanBeBackfilled = numberOfDocsCanSentToBackfillQueue, processingStrategy = ReprocessStrategy.BACKFILL)
        } else {
            val numberOfDocsCanSentToOperationalQueue = getNumberOfDocsCanBeProcessedWithOperationalQueue()
            if (numberOfDocsCanSentToOperationalQueue > 0){
                return ProcessingData(numberOfDocsCanBeBackfilled = numberOfDocsCanSentToOperationalQueue, processingStrategy = ReprocessStrategy.OPERATIONAL)
            }
        }

        return ProcessingData()
    }

    fun sendDocsToPipelineQueue(processingData: ProcessingData, documentData: List<BackfillDynamoRecords>) {
        println("Sending docs to pipeline queue $processingData")
        when (processingData.processingStrategy) {
            ReprocessStrategy.BACKFILL -> sendDocsToBackfillQueue(documentData, backfillQueueNameToSendMessages)
            ReprocessStrategy.OPERATIONAL -> sendDocsToOperationalQueue(documentData, operationalP2Queue)
        }
    }

    private fun getNumberOfDocsCanBeProcessedWithBackfillQueue(): Int{
        /*
        Prerequisite
            - Get number of message backfill queue can process at max per hour -> X
            - Get number of message indexing-low-priority queue can process at max per hour -> Y

        Rule for backfilling
            - each hour execute below rule
            - get number of messages in backfill queue(small + large) and indexing queue
                    - its non-business hours
                        - if number of messages in indexing queue is less than 10K
                              - if number of messages in backfill queue is less than 50K
                                    -> send X-50K documents to backfill queue

            -if its business hours
                - if number of messages in indexing queue is less than 3K
                    - if number of messages in backfill queue is less than 10K
                        -> send 20k documents to backfill queue


            - if weekday is Sat or Sun
                   - execute on each hour
                        - get number of messages in backfill queue(small + large) and indexing queue
                            - get number of messages in backfill queue(small + large) and indexing queue
                                - if number of messages in indexing queue is less than 10K
                                       - if number of messages iin backfill queue is less than 50K
                                            -> send X-50K
         */
        val maxNumberOfDocsToSendToBackfillQueue_NonBusinessHour = 200000
        val maxNumberOfDocsToSendToBackfillQueue_BusinessHour = 100000

        val thresholdBackfillQueue_NonBusinessHour = 100000
        val thresholdIndexingQueue_NonBusinessHour = 10000
        val thresholdBackfillQueue_BusinessHour = 50000
        val thresholdIndexingQueue_BusinessHour = 3000

        val numberOfMessagesInBackfillPreprocessorQueue = AwsSqsHelper.getQueueSize(backfillPreprocessorQueue)
        val numberOfMessagesInSmallBackfillQueue = AwsSqsHelper.getQueueSize(smallBackfillQueue)
        val numberOfMessagesInLargeBackfillQueue = AwsSqsHelper.getQueueSize(largeBackfillQueue)
        val numberOfMessagesInBackfillQueue = numberOfMessagesInSmallBackfillQueue + numberOfMessagesInLargeBackfillQueue + numberOfMessagesInBackfillPreprocessorQueue
        val numberOfMessagesInIndexingQueue = AwsSqsHelper.getQueueSize(indexingQueue)

        val numberOfMessagesInOperationalP1Queue = AwsSqsHelper.getQueueSize(operationalP1Queue)
        val numberOfMessagesInOperationalP2Queue = AwsSqsHelper.getQueueSize(operationalP2Queue)
        val numberOfMessagesInOperationalQueue = numberOfMessagesInOperationalP1Queue + numberOfMessagesInOperationalP2Queue
        println("#messages in Backfill Queue: $numberOfMessagesInBackfillQueue #messages in Indexing Queue: $numberOfMessagesInIndexingQueue")

        val queueMessagelog = "`BackfillQueueSize`: $numberOfMessagesInBackfillQueue `IndexingQueueSize`: ${numberOfMessagesInIndexingQueue} `OperationalQueueSize`: ${numberOfMessagesInOperationalQueue}\n"

        if (DateTimeHelper.isWeekday()){
            if (!DateTimeHelper.isUsBusinessHour()){
                //weekday non-business hour
                if (numberOfMessagesInIndexingQueue < thresholdIndexingQueue_NonBusinessHour && numberOfMessagesInBackfillQueue < thresholdBackfillQueue_NonBusinessHour){
                    val numberOfMessagesCanBeSent = (maxNumberOfDocsToSendToBackfillQueue_NonBusinessHour - numberOfMessagesInBackfillQueue).toInt()
                    messageToReport.append("${queueMessagelog}*$numberOfMessagesCanBeSent* docs can be sent to *backfill* queue as its weekday non-business hour")
                    return numberOfMessagesCanBeSent
                } else {
                    messageToReport.append("${queueMessagelog}_No docs can be sent to backfill queue as its weekday non-business hour and queues messages above threshold_")
                }
            } else {
                //weekday business hour
                if (numberOfMessagesInIndexingQueue < thresholdIndexingQueue_BusinessHour && numberOfMessagesInBackfillQueue < thresholdBackfillQueue_BusinessHour){
                    messageToReport.append("${queueMessagelog}*${maxNumberOfDocsToSendToBackfillQueue_BusinessHour}* docs can be sent to *backfill* queue as its weekday business hour")
                    return maxNumberOfDocsToSendToBackfillQueue_BusinessHour
                } else {
                    messageToReport.append("${queueMessagelog}_No docs can be sent to backfill queue as its weekday business hour and queue messages above threshold_")
                }
            }
        } else {
            //weekend
            if (numberOfMessagesInIndexingQueue < thresholdIndexingQueue_NonBusinessHour && numberOfMessagesInBackfillQueue < thresholdBackfillQueue_NonBusinessHour){
                val numberOfMessagesCanBeSent = (maxNumberOfDocsToSendToBackfillQueue_BusinessHour - numberOfMessagesInBackfillQueue).toInt()
                messageToReport.append("${queueMessagelog}*$numberOfMessagesCanBeSent* docs can be sent to *backfill* queue as its weekend")
                return (maxNumberOfDocsToSendToBackfillQueue_NonBusinessHour - numberOfMessagesInBackfillQueue).toInt()
            } else {
                messageToReport.append("${queueMessagelog}_No docs can be sent to backfill queue as its weekend and queue messages above threshold_")
            }
        }

        println(messageToReport)
        return 0
    }

    private fun getNumberOfDocsCanBeProcessedWithOperationalQueue(): Int{
        /*
        Rule for backfilling
            - each hour execute below rule
            - get number of messages in backfill queue(small + large) and indexing queue
                    - its non-business hours
                        - if number of messages in indexing queue is less than 10K
                              - if number of messages in backfill queue is less than 50K
                                    -> send X-50K documents to backfill queue

            -if its business hours
                - if number of messages in indexing queue is less than 3K
                    - if number of messages in backfill queue is less than 10K
                        -> send 20k documents to backfill queue


            - if weekday is Sat or Sun
                   - execute on each hour
                        - get number of messages in backfill queue(small + large) and indexing queue
                            - get number of messages in backfill queue(small + large) and indexing queue
                                - if number of messages in indexing queue is less than 10K
                                       - if number of messages iin backfill queue is less than 50K
                                            -> send X-50K
         */
        val maxNumberOfDocsToSendToOperationalQueue_NonBusinessHour = 70000
        val maxNumberOfDocsToSendToOperationalQueue_BusinessHour = 7000

        val thresholdOperationalQueue_NonBusinessHour = 30000
        val thresholdIndexingQueue_NonBusinessHour = 5000
        val thresholdOperationalQueue_BusinessHour = 2000
        val thresholdIndexingQueue_BusinessHour = 1000

        val numberOfMessagesInOperationalP1Queue = AwsSqsHelper.getQueueSize(operationalP1Queue)
        val numberOfMessagesInOperationalP2Queue = AwsSqsHelper.getQueueSize(operationalP2Queue)
        val numberOfMessagesInOperationalQueue = numberOfMessagesInOperationalP1Queue + numberOfMessagesInOperationalP2Queue
        val numberOfMessagesInIndexingQueue = AwsSqsHelper.getQueueSize(indexingQueue)
        println("#messages in Operational Queue: $numberOfMessagesInOperationalQueue #messages in Indexing Queue: $numberOfMessagesInIndexingQueue")

        if (DateTimeHelper.isWeekday()){
            if (!DateTimeHelper.isUsBusinessHour()){
                //weekday non-business hour
                if (numberOfMessagesInIndexingQueue < thresholdIndexingQueue_NonBusinessHour && numberOfMessagesInOperationalQueue < thresholdOperationalQueue_NonBusinessHour){
                    val numberOfMessagesCanBeSent = (maxNumberOfDocsToSendToOperationalQueue_NonBusinessHour - numberOfMessagesInOperationalQueue).toInt()
                    messageToReport.append("\n*$numberOfMessagesCanBeSent* docs can be sent to *operational* queue as its weekday non-business hour")
                    return numberOfMessagesCanBeSent
                } else {
                    messageToReport.append("\n_No docs can be sent to operational queue as its weekday non-business hour and queues messages above threshold_")
                }
            } else {
                //weekday business hour
                if (numberOfMessagesInIndexingQueue < thresholdIndexingQueue_BusinessHour && numberOfMessagesInOperationalQueue < thresholdOperationalQueue_BusinessHour){
                    val numberOfMessagesToBeSent = (maxNumberOfDocsToSendToOperationalQueue_BusinessHour - numberOfMessagesInOperationalQueue).toInt()
                    messageToReport.append("\n*$numberOfMessagesToBeSent* docs can be sent to *operational* queue as its weekday business hour")
                    return numberOfMessagesToBeSent
                } else {
                    messageToReport.append("\n_No docs can be sent to operational queue as its weekday business hour and queue messages above threshold_")
                }
            }
        } else {
            //weekend
            if (numberOfMessagesInIndexingQueue < thresholdIndexingQueue_NonBusinessHour && numberOfMessagesInOperationalQueue < thresholdOperationalQueue_NonBusinessHour){
                val numberOfMessagesCanBeSent = (maxNumberOfDocsToSendToOperationalQueue_NonBusinessHour - numberOfMessagesInOperationalQueue).toInt()
                messageToReport.append("\n*$numberOfMessagesCanBeSent* docs can be sent to *operational* queue as its weekend")
                return numberOfMessagesCanBeSent
            } else {
                messageToReport.append("\n_No docs can be sent to operational queue as its weekend and queue messages above threshold_")
            }
        }

        return 0
    }

    private fun sendDocsToBackfillQueue(documentData: List<BackfillDynamoRecords>, queueNameToSendMessages: String) {
        println("Sending ${documentData.size} docs to the backfill queue: $queueNameToSendMessages")
        try {
            val messageAttributes = mapOf(
                "feedType" to MessageAttributeValue()
                    .withDataType("String").withStringValue("HISTORY")
            )

            // Define the batch size
            val batchSize = 10

            // Partition the list of document IDs into batches
            val documentBatches = documentData.chunked(batchSize)

            documentBatches.parallelStream().forEach { batch ->
                val batchRequestEntries = batch.map { doc ->
                    val docType = if (doc.docType.isNullOrBlank()){
                        "Backfill"
                    } else {
                        doc.docType
                    }
                    val requestBodyAsString = """{
                "Type" : "Notification",
                "MessageId" : "${UUID.randomUUID()}",
                "TopicArn" : "arn:aws:sns:us-east-1:752320408524:DocumentProcessingTest",
                "Message" : "{\"documentDate\":\"2024-07-13T05:54:46\",\"documentType\":\"${docType}\",\"action\":\"CREATE\",\"accessionNumber\":\"${doc.id}\"}",
                "Timestamp" : "2024-07-08T06:12:46.261Z",
                "SignatureVersion" : "1",
                "Signature" : "WLnherpqkshT/ZKB/WRBvTbf3i6jOVDc/z0vaI5M4mOpYp00qicNK7y/7k8p3/nSnG4MLNvxRGyKAoxv3JwhsepQzMUIjLCAH8hBK68UkksXSE0dXopass1c/ipQ/Zt3vpDVX1Jc2Z3/lhrGutxoHd9TX8tF+Nktc7lO43bnxDkxuBRl1OdyOEtHuY+SQkA40+W7SHnli5z0MkWswSWcwousciVo2MoPfFe8vhx9XtWXRMqTRD6wIDPxDexXrrv3ScMBh8QQuqeaxY0bAcb6DVZF6q1Yph3XvF39bXu/2ft7uBDxjvvxIsD9PBt3g33jmJyjcPl3Zo9GM+Htta4Ulw==",
                "SigningCertURL" : "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-7ff5318490ec183fbaddaa2a969abfda.pem",
                "UnsubscribeURL" : "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:752320408524:DocumentProcessingTest:f5746563-4300-40d4-a6a2-704dff8ef25b"
                }""".trimIndent()

                    SendMessageBatchRequestEntry(UUID.randomUUID().toString(), requestBodyAsString)
                        .withMessageAttributes(messageAttributes)
                }

                val sendBatchRequest = SendMessageBatchRequest()
                    .withQueueUrl(queueNameToSendMessages)
                    .withEntries(batchRequestEntries)

                AwsClient.sqsClient.sendMessageBatch(sendBatchRequest)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        println("Sent ${documentData.size} docs to the backfill queue: $queueNameToSendMessages")
    }

    private fun sendDocsToOperationalQueue(documentData: List<BackfillDynamoRecords>, queueNameToSendMessages: String) {
        println("Sending ${documentData.size} docs to the operational queue: $queueNameToSendMessages")
        try {
            val messageAttributes = mapOf(
                "feedType" to MessageAttributeValue()
                    .withDataType("String").withStringValue("HISTORY")
            )

            // Define the batch size
            val batchSize = 10

            // Partition the list of document IDs into batches
            val documentBatches = documentData.chunked(batchSize)

            documentBatches.parallelStream().forEach { batch ->
                val batchRequestEntries = batch.map { doc ->
                    val requestBodyAsString = """
                        {
                        "documentId": "${doc.id}",
                        "type":"ADD_OR_UPDATE",
                        "steps":["ALL"],
                        "asIdUpdateEvent": null
                        }
                        """.trimIndent()

                    SendMessageBatchRequestEntry(UUID.randomUUID().toString(), requestBodyAsString)
                        .withMessageAttributes(messageAttributes)
                }

                val sendBatchRequest = SendMessageBatchRequest()
                    .withQueueUrl(queueNameToSendMessages)
                    .withEntries(batchRequestEntries)

                AwsClient.sqsClient.sendMessageBatch(sendBatchRequest)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        println("Sent ${documentData.size} docs to the operational queue: $queueNameToSendMessages")
    }
}

data class ExchangesData(
    val exchange: String? = null,
    val priority: Int = 100
)

data class ProcessingData(
    val numberOfDocsCanBeBackfilled: Int = 0,
    val processingStrategy: ReprocessStrategy = ReprocessStrategy.BACKFILL
)

enum class ReprocessStrategy {
    BACKFILL,
    OPERATIONAL
}