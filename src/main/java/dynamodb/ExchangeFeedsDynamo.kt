package dynamodb

import clients.AwsClient
import com.amazonaws.services.dynamodbv2.datamodeling.*
import com.amazonaws.services.dynamodbv2.model.*


class ExchangeFeedsDynamo {
    private val dynamoTable = "downloader_backfill_feeds_documents"

    fun getNotBackfilledDocs(exchange: String?, limit: Int): List<BackfillDynamoRecords> {
        return if (exchange == null){
            getNotBackfilledDocsForAllExchange(limit)
        } else {
            getNotBackfilledDocsForGivenExchange(exchange, limit)
        }
    }


    private fun getNotBackfilledDocsForAllExchange(limit: Int): List<BackfillDynamoRecords> {
        println("Getting not backfilled records for All exchanges")
        val foundDoc = mutableListOf<BackfillDynamoRecords>()
        val expressionAttributeValues = mutableMapOf<String, AttributeValue>(
            ":isBackfilledVal" to AttributeValue().withS("false"),
            ":updateDateTimeVal" to AttributeValue().withS("2020-08-17T00:00:00"),
        )

        val mapper = DynamoDBMapper(AwsClient.dynamoClient)

        val queryExpression = DynamoDBQueryExpression<BackfillDynamoRecords>()
            .withIndexName("isBackfilledIndex") // Replace with your actual index name
            .withKeyConditionExpression("isBackfilled = :isBackfilledVal and updateDatetime > :updateDateTimeVal")
            .withExpressionAttributeValues(expressionAttributeValues)
            .withLimit(1000)
            .withConsistentRead(false) // Secondary indexes support only eventual consistency

        var lastEvaluatedKey: Map<String, AttributeValue>? = null

        do {
            val queryResult = mapper.queryPage(BackfillDynamoRecords::class.java, queryExpression.withExclusiveStartKey(lastEvaluatedKey))
            lastEvaluatedKey = queryResult.lastEvaluatedKey
            foundDoc.addAll(queryResult.results)
        } while (lastEvaluatedKey != null && foundDoc.size < limit)

        if (foundDoc.size < 5) return emptyList()

        return foundDoc
    }

    private fun getNotBackfilledDocsForGivenExchange(exchange: String?, limit: Int): List<BackfillDynamoRecords> {
        println("Getting not backfilled records for exchange $exchange")
        val foundDoc = mutableListOf<BackfillDynamoRecords>()
        val expressionAttributeValues = mutableMapOf<String, AttributeValue>(
            ":isBackfilledVal" to AttributeValue().withS("false"),
            ":exchangeVal" to AttributeValue().withS(exchange)
        )

        val mapper = DynamoDBMapper(AwsClient.dynamoClient)

        val queryExpression = DynamoDBQueryExpression<BackfillDynamoRecords>()
            .withIndexName("vendorNameIndex") // Replace with your actual index name
            .withKeyConditionExpression("vendorName = :exchangeVal and isBackfilled = :isBackfilledVal")
            .withExpressionAttributeValues(expressionAttributeValues)
            .withLimit(1000)
            .withConsistentRead(false) // Secondary indexes support only eventual consistency

        var lastEvaluatedKey: Map<String, AttributeValue>? = null

        do {
            val queryResult = mapper.queryPage(BackfillDynamoRecords::class.java, queryExpression.withExclusiveStartKey(lastEvaluatedKey))
            lastEvaluatedKey = queryResult.lastEvaluatedKey
            foundDoc.addAll(queryResult.results)
        } while (lastEvaluatedKey != null && foundDoc.size <= limit)

        if (foundDoc.size < 5) return emptyList()

        return foundDoc
    }

    fun updateBackfilledRecords(recordsToUpdate: List<BackfillDynamoRecords>){
        val ttlInSeconds = 10
        //update ttl and backfill status
        recordsToUpdate.parallelStream().forEach { record ->
            val updateItemRequest = UpdateItemRequest().apply {
                tableName = dynamoTable
                key = mapOf("docId" to AttributeValue().withS(record.id), "createdAt" to AttributeValue().withS(record.creationDatetime)) // Primary key of the item to update
                attributeUpdates = mapOf(
                    "isBackfilled" to AttributeValueUpdate().apply {
                        action = AttributeAction.PUT.name// Use PUT to add or replace the attribute
                        value = AttributeValue().withS("TRUE")
                    },

                    "TimeToLive" to AttributeValueUpdate().apply {
                        action = AttributeAction.PUT.name
                        value = AttributeValue().withN((System.currentTimeMillis() / 1000 + ttlInSeconds).toString())
                    }
                )
            }

            AwsClient.dynamoClient.updateItem(updateItemRequest)
        }
    }

    fun updateDynamoRecordsInBatch(recordsToUpdate: List<BackfillDynamoRecords>) {
        println("Updating dynamo records in batch for ${recordsToUpdate.size} records")
        val ttlInSeconds = 86400
        val batchSize = 25

        val batches = recordsToUpdate.chunked(batchSize)
        batches.forEach { batch ->
            val writeRequests = batch.map { record ->
                WriteRequest().withPutRequest(
                    PutRequest().withItem(
                        mapOf(
                            "id" to AttributeValue().withS(record.id),
                            "docType" to AttributeValue().withS(record?.docType?:"UNKNOWN"),
                            "creationDatetime" to AttributeValue().withS(record.creationDatetime),
                            "updateDatetime" to AttributeValue().withS(record.updateDatetime),
                            "vendorName" to AttributeValue().withS(record?.vendorName?:"UNKNOWN"),
                            "isBackfilled" to AttributeValue().withS("true"),
                            "fileName" to AttributeValue().withS(record?.fileName?:""),
                            "expiryTimestamp" to AttributeValue().withN((System.currentTimeMillis() / 1000 + ttlInSeconds).toString())
                        )
                    )
                )
            }

            val batchWriteItemRequest = BatchWriteItemRequest().withRequestItems(
                mapOf(dynamoTable to writeRequests)
            )

            try {
                val response = AwsClient.dynamoClient.batchWriteItem(batchWriteItemRequest)
                if (response.unprocessedItems.isNotEmpty()){
                    println("unprocessed items -> ${response.unprocessedItems}")
                    println("Adding sleep for 20 second as there are unprocessed items and retrying...")
                    Thread.sleep(20000)
                    AwsClient.dynamoClient.batchWriteItem(batchWriteItemRequest)
                }
            } catch (e: Exception) {
                println("Failed to update batch: ${e.message}")
            }
        }
        println("Updated dynamo records in batch for ${recordsToUpdate.size} records")
    }

}

@DynamoDBTable(tableName = "downloader_backfill_feeds_documents")
class BackfillDynamoRecords {
    @get:DynamoDBHashKey(attributeName = "id")
    var id: String? = null

    @get:DynamoDBAttribute(attributeName = "vendorName")
    var vendorName: String? = null

    @get:DynamoDBAttribute(attributeName = "docType")
    var docType: String? = null

    @get:DynamoDBAttribute(attributeName = "creationDatetime")
    var creationDatetime: String? = null

    @get:DynamoDBAttribute(attributeName = "updateDatetime")
    var updateDatetime: String? = null

    @get:DynamoDBAttribute(attributeName = "isBackfilled")
    var isBackfilled: String? = null

    @get:DynamoDBAttribute(attributeName = "fileName")
    var fileName: String? = null
}

