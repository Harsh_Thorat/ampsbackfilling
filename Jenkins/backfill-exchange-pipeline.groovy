#!groovy

import groovy.json.JsonSlurper
import hudson.Util
import hudson.tasks.junit.CaseResult
import hudson.tasks.test.AbstractTestResultAction


/*********************************
 *    Basic initialization
 **********************************/

def buildDetails = env.JOB_NAME.tokenize('/') as String[]
def buildName = buildDetails[1]
def branch = scm.branches.first().getName().replace("*/", "")
def slave = env.JOB_BASE_NAME
def environment = env.JOB_BASE_NAME
def noDownloadUploadLogsArgs = "-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn"
def started_by_user = currentBuild.getBuildCauses()[0]["shortDescription"].toString().replace("Started by user","").trim()

/*********************************
 *    Tests schedule
 **********************************/

def testSchedule = "H/60 * * * *"

/*********************************
 *    Jenkins build properties
 **********************************/

properties([
        pipelineTriggers([[$class: "TimerTrigger", spec: testSchedule]]),
        buildDiscarder(logRotator(artifactDaysToKeepStr: '90', artifactNumToKeepStr: '30', daysToKeepStr: '90', numToKeepStr: '30')),
        parameters([
                string(name: "SlackChannel", defaultValue: "DEFAULT", description: 'Add Channel or user name to notify about success of job ex. @hthorat,#team-prithvi'),
                string(name: "exchange", defaultValue: "", description: 'Provide vendorName to backfill documents for that exchange.'),
        ])
])

/*********************************
 *    Configuration
 **********************************/

def tagsToRun = "'BackfillExchange'"

slave = "production"

/*********************************
 *    Slack reporting
 **********************************/

def slackChannel = "$SlackChannel".toString()

if ("$SlackChannel".toString() == "DEFAULT") {
    slackChannel = "#alerts-feeds-backfill"
}

/*********************************
 *    Pipeline
 **********************************/
timestamps {
    node("master") {
        stage('Starting the tests') {
            echo "Pipeline is running with following parameters:\nBuild name: [$buildName]\nBranch: [$branch]\n" +
                    "Slave: [$slave]\nSlackChannel: [$SlackChannel]\nExchange: [$exchange]\n"
        }
    }

    node("$slave") {
        stage('Git checkout') {
            git(
                    url: 'git@bitbucket.org:Harsh_Thorat/AMPSBackfilling.git',
                    credentialsId: 'infra-reader.pem',
                    branch: branch
            )
        }
        stage('Backfill Documents') {
            sh "mvn $noDownloadUploadLogsArgs -B test -Dgroups='$tagsToRun' -Dtest.exchange='$exchange' -Dtest.startedByUser='$started_by_user' || exit 0"
        }

        stage('Reporting') {
            try {
                def content = readFile 'reporting.txt'
                slackSend(channel: slackChannel, color: '#2D953E', message: "$content ")
            } catch (err) {
                echo 'Error for backfilling job.' + err.message
                slackSend(channel: slackChannel, color: '#FF0000', message: "ALERT: Some issue with doc backfilling job. Please contact @hthorat.")
            }
        }
    }
}