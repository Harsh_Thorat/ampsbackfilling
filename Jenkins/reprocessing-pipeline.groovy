#!groovy

import groovy.json.JsonSlurper
import hudson.Util
import hudson.tasks.junit.CaseResult
import hudson.tasks.test.AbstractTestResultAction


/*********************************
 *    Basic initialization
 **********************************/

def buildDetails = env.JOB_NAME.tokenize('/') as String[]
def buildName = buildDetails[1]
def branch = scm.branches.first().getName().replace("*/", "")
def slave = env.JOB_BASE_NAME
def environment = env.JOB_BASE_NAME
def noDownloadUploadLogsArgs = "-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn"
def started_by_user = currentBuild.getBuildCauses()[0]["shortDescription"].toString().replace("Started by user","").trim()

/*********************************
 *    Tests schedule
 **********************************/

def testSchedule = "H H 31 2 7"

/*********************************
 *    Jenkins build properties
 **********************************/

properties([
        pipelineTriggers([[$class: "TimerTrigger", spec: testSchedule]]),
        buildDiscarder(logRotator(artifactDaysToKeepStr: '90', artifactNumToKeepStr: '30', daysToKeepStr: '90', numToKeepStr: '30')),
        parameters([
                string(name: "SlackChannel", defaultValue: "", description: 'Add Channel or user name to notify about success of job ex. @hthorat,#team-prithvi'),
                choice(name: "TestEnvironment", choices: ['dev','rc', 'prod'], description: 'Environment in which you want to reprocess'),
                choice(name: "queuePriority", choices: ['Priority1','Priority2'], description: 'Queue Priority in which message will be sent'),
                string(name: "filename", defaultValue: "", description: 'Upload file in bucket as.cd.missingdoc and reprocessing folder and provide name in this param'),
                string(name: "docIds", defaultValue: "", description: 'Pass documents(multiple comma separated) which need to be processed, if filename given this will be ignored'),
                extendedChoice(
                        name: 'DTMSteps',
                        defaultValue: 'ALL',
                        description: 'Select Steps for reprocessing. Default value is ALL',
                        type: 'PT_CHECKBOX',
                        value: 'ALL,HTML_TABLE_EXTRACTION,BLACKLINE,SIMILAR_TABLE_EXTRACTION,SECTION_SEARCH,POSTING_HANDLER,DEFAULT_REPROCESSING,CNR_REPROCESSING,REPROCESSING_SKIP_CNR',
                        visibleItemCount: 12
                ),
        ])
])

/*********************************
 *    Configuration
 **********************************/

def tagsToRun = "'ReprocessDocuments'"

environment = TestEnvironment.toString()

switch (environment) {
    case "dev":
        slave = "development"
        break
    case "rc":
        slave = "staging"
        break
    case "prod":
        slave = "production"
        break
    default:
        slave = "$TestEnvironment".toString()
}

/*********************************
 *    Slack reporting
 **********************************/

def slackChannel = "$SlackChannel".toString()

if ("$SlackChannel".toString() == "DEFAULT") {
    slackChannel = "@hthorat"
}

/*********************************
 *    Pipeline
 **********************************/
timestamps {
    node("master") {
        stage('Starting the tests') {
            echo "Pipeline is running with following parameters:\nBuild name: [$buildName]\nBranch: [$branch]\n" +
                    "Slave: [$slave]\nSlackChannel: [$SlackChannel]\n" +
                    "Environment: [$TestEnvironment]\n" +
                    "filename: [$filename]\n" +
                    "steps: [$DTMSteps]\n" +
                    "docId: [$docIds]\n" +
                    "filename: [$filename]\n"
            slackSend(channel: "$slackChannel", color: '#00BFFF', message: "Started reprocessing by $started_by_user for \n```File: $filename \nDocIds:$docIds \nEnv: $TestEnvironment \nQueue: $queuePriority \nSteps: $DTMSteps```")
        }
    }

    node("$slave") {
        stage('Git checkout') {
            git(
                    url: 'git@bitbucket.org:Harsh_Thorat/AMPSBackfilling.git',
                    credentialsId: 'infra-reader.pem',
                    branch: branch
            )
        }
        stage('Reprocess Documents') {
            sh "mvn $noDownloadUploadLogsArgs -B test -Dgroups='$tagsToRun' -Dtest.env='$environment' -Dtest.queuePriority='$queuePriority' -Dtest.filename='$filename' -Dtest.docIds='$docIds' -Dtest.steps='$DTMSteps'|| exit 0"
        }

        stage('Reporting') {
            try {
                def content = readFile 'reporting.txt'
                slackSend(channel: slackChannel, color: '#2D953E', message: "$content ")
            } catch (err) {
                echo 'Error for reprocessing job.' + err.message
                slackSend(channel: slackChannel, color: '#FF0000', message: "ALERT: Some issue with doc reprocessing job. Please contact @hthorat.")
            }
        }
    }
}