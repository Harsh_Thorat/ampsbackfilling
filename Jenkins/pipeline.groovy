#!groovy

import groovy.json.JsonSlurper
import hudson.Util
import hudson.tasks.junit.CaseResult
import hudson.tasks.test.AbstractTestResultAction


/*********************************
 *    Basic initialization
 **********************************/

def buildDetails = env.JOB_NAME.tokenize('/') as String[]
def buildName = buildDetails[1]
def branch = scm.branches.first().getName().replace("*/", "")
def slave = env.JOB_BASE_NAME
def environment = env.JOB_BASE_NAME
def text = env.text

/*********************************
 *    Tests schedule
 **********************************/

def testSchedule = "H H 31 2 7"

/*********************************
 *    Tests mapping
 **********************************/

def runTagsLibrary = [
        //features
        "get-transcripts-metadata"                 : "'get-transcripts-metadata'"
]

/*********************************
 *    Jenkins build properties
 **********************************/

properties([
        pipelineTriggers([[$class: "TimerTrigger", spec: testSchedule]]),
        buildDiscarder(logRotator(artifactDaysToKeepStr: '14', artifactNumToKeepStr: '10', daysToKeepStr: '14', numToKeepStr: '10')),
        parameters([
                string(name: "SlackChannel", defaultValue: "@hthorat", description: 'DEFAULT option means that development results will go to #cd-pipelines-*** or to #qa-be-automation depending on job name'),
                string(name: 'TestEnvironment', defaultValue: "master", description: 'APPLICABLE ONLY FOR FEATURE BRANCHES & MINISTACKS. Define test environment like: \'development\', \'staging\', \'master\' or some ministack name'),
                choice(name: "Tags", choices: [ 'ReindexGF','BackfillTR', 'AMPSBackfill', 'AMPSBackfillByVendorId', 'MoveMessages'], description: 'APPLICABLE ONLY FOR FEATURE BRANCHES. Define tags to run tests like: \'document-data,doc-search\' or \'type.Smoke\''),
                string(name: "channel_name", defaultValue: "", description: 'Param for remote triggering, just keep it empty'),
                string(name: "user_name", defaultValue: "", description: 'Param for remote triggering, just keep it empty'),
                string(name: "text", defaultValue: "", description: "Param for remote triggering by slack command, just keep it empty")
        ])
])

/*********************************
 *    Configuration
 **********************************/

def tagsToRun = "$Tags".toString()
if (runTagsLibrary.containsKey(buildName.toString())) {
    tagsToRun = runTagsLibrary.get(buildName)
}

environment = TestEnvironment.toString()

switch (environment) {
    case "development":
        slave = "development"
        break
    case "staging":
        slave = "staging"
        break
    case "master":
        slave = "production"
        break
    default:
        slave = "$TestEnvironment".toString()
}

/*********************************
 *    Slack reporting
 **********************************/

def slackChannel = "$SlackChannel".toString()

if ("$channel_name".toString() != "") {
    if ("$channel_name".toString() == "directmessage") {
        slackChannel = "@${user_name}"
    } else {
        slackChannel = "#${channel_name}"
    }
} else if ("$SlackChannel".toString() == "DEFAULT") {
    slackChannel = "@hthorat"
}

/*********************************
 *    Pipeline
 **********************************/
timestamps {
    node("master") {
        stage('Starting the tests') {
            echo "Pipeline is running with following parameters:\nBuild name: [$buildName]\nBranch: [$branch]\n" +
                    "Environment: [$environment]\nTags to run: [$tagsToRun]\n" +
                    "Slave: [$slave]\nSlackChannel: [$SlackChannel]\nchannel_name: [$channel_name]\nuser_name: [$user_name]\n" +
                    "Slack_Parameter: [$text]\n"
            if (text != ""){
                slackSend(channel: "$slackChannel", color: '#00BFFF', message: "Started backfilling for $text :agree:")
            } else
                slackSend(channel: "$slackChannel", color: '#00BFFF', message: "Started backfilling :agree:")
        }
    }

    node("$slave") {
        stage('Git checkout') {
            git(
                    url: 'git@bitbucket.org:Harsh_Thorat/AMPSBackfilling.git',
                    credentialsId: 'infra-reader.pem',
                    branch: branch
            )
        }
        stage('Backfill Documents') {
            sh "mvn test -Dgroups='$tagsToRun' -Dtest.env='$environment' -Dtest.text='$text'|| exit 0"
        }

        stage('Reporting') {
            if (text != ""){
                slackSend(channel: slackChannel, color: '#FFCE42', message: "Backfilling completed for $text")
            } else
                slackSend(channel: slackChannel, color: '#FFCE42', message: "Backfilling Successful")
        }
    }
}