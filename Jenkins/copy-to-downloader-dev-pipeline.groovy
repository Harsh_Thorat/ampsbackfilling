#!groovy

import groovy.json.JsonSlurper
import hudson.Util
import hudson.tasks.junit.CaseResult
import hudson.tasks.test.AbstractTestResultAction


/*********************************
 *    Basic initialization
 **********************************/

def buildDetails = env.JOB_NAME.tokenize('/') as String[]
def buildName = buildDetails[1]
def branch = scm.branches.first().getName().replace("*/", "")
def slave = env.JOB_BASE_NAME
def environment = env.JOB_BASE_NAME
def noDownloadUploadLogsArgs = "-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn"
def started_by_user = currentBuild.getBuildCauses()[0]["shortDescription"].toString().replace("Started by user","").trim()

/*********************************
 *    Tests schedule
 **********************************/

def testSchedule = "H H 31 2 7"

/*********************************
 *    Jenkins build properties
 **********************************/

properties([
        pipelineTriggers([[$class: "TimerTrigger", spec: testSchedule]]),
        buildDiscarder(logRotator(artifactDaysToKeepStr: '90', artifactNumToKeepStr: '30', daysToKeepStr: '90', numToKeepStr: '30')),
        parameters([
                string(name: "SlackChannel", defaultValue: "", description: 'Add Channel or user name to notify about success of job ex. @hthorat,#team-prithvi'),
                string(name: "filename", defaultValue: "", description: 'Upload file in bucket as.cd.missingdoc and reprocessing folder and provide name in this param'),
                string(name: "docIds", defaultValue: "", description: 'Pass documents(multiple comma separated) which need to be processed, if filename given this will be ignored')
        ])
])

/*********************************
 *    Configuration
 **********************************/

def tagsToRun = "'CopyToDownloaderDev'"

slave = "development"

/*********************************
 *    Slack reporting
 **********************************/

def slackChannel = "$SlackChannel".toString()

if ("$SlackChannel".toString() == "DEFAULT") {
    slackChannel = "@hthorat"
}

/*********************************
 *    Pipeline
 **********************************/
timestamps {
    node("master") {
        stage('Starting the tests') {
            echo "Pipeline is running with following parameters:\nBuild name: [$buildName]\nBranch: [$branch]\n" +
                    "Slave: [$slave]\nSlackChannel: [$SlackChannel]\n" +
                    "filename: [$filename]\n" +
                    "docId: [$docIds]\n" +
            slackSend(channel: "$slackChannel", color: '#00BFFF', message: "Started reprocessing by $started_by_user for \n```File: $filename \nDocIds:$docIds```")
        }
    }

    node("$slave") {
        stage('Git checkout') {
            git(
                    url: 'git@bitbucket.org:Harsh_Thorat/AMPSBackfilling.git',
                    credentialsId: 'infra-reader.pem',
                    branch: branch
            )
        }
        stage('Reprocess Documents') {
            sh "mvn $noDownloadUploadLogsArgs -B test -Dgroups='$tagsToRun' -Dtest.filename='$filename' -Dtest.docIds='$docIds' || exit 0"
        }

        stage('Reporting') {
            try {
                def content = readFile 'reporting.txt'
                slackSend(channel: slackChannel, color: '#2D953E', message: "$content ")
            } catch (err) {
                echo 'Error for reprocessing job.' + err.message
                slackSend(channel: slackChannel, color: '#FF0000', message: "ALERT: Some issue with doc copy to down-dev job. Please contact @hthorat.")
            }
        }
    }
}